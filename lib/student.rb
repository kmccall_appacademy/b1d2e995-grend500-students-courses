class Student
  attr_reader :first_name, :last_name, :courses
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []

  end

  def name
    "#{@first_name } #{@last_name}"
  end

  def enroll(course)
    return if @courses.include? course
    raise 'course conflict' if conflict?(course)
    @courses << course
    course.students << self
  end

  def conflict?(course)
    @courses.any? { |c| course.conflicts_with?(c) }
  end

  def course_load
    load = Hash.new(0)
    @courses.each { |course| load[course.department] += course.credits }
    load
  end

end
